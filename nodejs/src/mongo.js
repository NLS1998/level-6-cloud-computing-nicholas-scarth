const mongoose = require('mongoose');

const express = require('express')

const bodyParser = require('body-parser');

request = require('request');

const app = express()
const port = 3000

const connectionString = 'mongodb://localmogo1:27017,localmongo2:27017,localmongo3:27017/sweetShopDB?replicaSet=cfgrs';

systemLeader = 0;

var os = require("os");
var myHostName = os.hostname();
var url = 'http://192.168.56.111:2375';
var request = require('request');

const fs = require('fs');
nodesTxtFile = fs.readFileSync('node.js');
nodes = JSON.parse(nodesTxtFile);

var amqp = require('amqplib/callback_api');

var randomNumber = Math.floor(Math.random() * (1000 - 1 + 1) + 1);
toSend = {"hostName" : myHostName, "message": "I am ALIVE","randomNumber":randomNumber} ;

let allTheNodes = [];

setInterval (function () {
amqp.connect('amqp://test:test@192.168.56.111', function(error0, connection){
if (error0) {
        throw error0;
      }
      connection.createChannel(function(error1, channel) {
              if (error1) {
                        throw error1;
                      }
	      var exchange = 'logs';
	      var message = JSON.stringify(toSend);

              channel.assertExchange(exchange, 'fanout', {
                        durable: false
                      });
	      channel.publish(exchange, '', Buffer.from(message));
	      });
	});
}, 10000);

amqp.connect('amqp://test:test@192.168.56.111', function (error0, connection) {
      if (error0) {
              throw error0;
            }
      connection.createChannel(function(error1, channel) {
              if (error1) {
                        throw error1;
                      }
              var exchange = 'logs';

              channel.assertExchange(exchange, 'fanout', {
                        durable: false
                      });

              channel.assertQueue('', {
                        exclusive: true
                      }, function(error2, q) {
                                if (error2) {
                                            throw error2;
                                          }
                                console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
                                channel.bindQueue(q.queue, exchange, '');

                                channel.consume(q.queue, function(message) {
                                            if(message.content) {
						    var messageString = message.content.toString();
						    var messageParse = JSON.parse(messageString);
						console.log(messageParse.hostName, " is ALIVE!!!");
						allTheNodes.some(oneNode => oneNode.hostName === messageParse.hostName) ? (allTheNodes.find(n => n.hostName === messageParse.hostName)).timeStamp = Date.now() : allTheNodes.push({"hostName":messageParse.hostName,"ID":messageParse.randomNumber,"timeStamp":Date.now(),"leader":0});						    
					    }
                                          }, {
                                                      noAck: true
                                                    });
                              });
            });
});

var numbers = 0;

setInterval (function () {
	 allTheNodes.forEach(function (oneNode) {
		if (numbers < oneNode.ID) {
		numbers = oneNode.ID;
		}
	 })	
	allTheNodes.some(oneNode => oneNode.ID === numbers) ? (allTheNodes.find(n => n.ID === numbers)).leader = 1 : (allTheNodes.find(n => n.ID !== numbers)).leader = 0;
			 
	allTheNodes.some(oneNode => oneNode.leader === 1 && oneNode.hostName === myHostName) ?
 
     	console.log("Hello this is " + myHostName + " I AM the leader") : console.log("Hello this is " + myHostName + " I am NOT the leader");
}, 20000);


var create = {
    uri: url + "/v1.41/containers/create",
        method: 'POST',
        json: {"Image": "level-6-cloud-computing-nicholas-scarth_node3", "Cmd": ["echo", "This is a hello from the new container. R.I.P the old container."]}
};

setInterval (function () {
request(create, function (error, response, createBody) {	
	allTheNodes.forEach(function (oneNode) {
		if (Date.now() - oneNode.timeStamp > 60000) {
//			if (allTheNodes.find(oneNode => oneNode.hostName === myHostName).leader = 0) {
			if (!error) {
            console.log("Created container " + JSON.stringify(createBody));

        var start = {
            uri: url + "/v1.41/containers/" + createBody.Id + "/start",
                method: 'POST',
                json: {"Image": "level-6-cloud-computing-nicholas-scarth_node3", "Cmd": ["echo", "This is a hello from the new container. R.I.P the old container."]}
            };

        request(start, function (error, response, startBody) {
                if (!error) {
                        console.log("Container start completed");

                var wait = {
                                uri: url + "/v1.40/containers/" + createBody.Id + "/wait",
                    method: 'POST',
                            json: {}
                        };


                            request(wait, function (error, response, waitBody ) {
                                if (!error) {
                                        console.log("run wait complete, container will have started");

                        request.get({
                            url: url + "/v1.40/containers/" + createBody.Id + "/logs?stdout=1",
                            }, (err, res, data) => {
                                    if (err) {
                                        console.log('Error:', err);
                                    } else if (res.statusCode !== 200) {
                                        console.log('Status:', res.statusCode);
                                    } else{
                                        console.log("Container stdout = " + data);
					    numbers = 0;
						allTheNodes.push({"hostName":oneNode.hostName,"ID":randomNumber,"timeStamp":Date.now(),"leader":0});
				    }
                                });
                        }
                        });
            }
        });
				}
   
};
});
});
}, 90000);

app.use(bodyParser.json());

mongoose.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var Schema = mongoose.Schema;

var videoSchema = new Schema({
  AccountID: Number,
  UserName: String,
  TitleID: String,
  UserAction: String,
  DateAndTime: Date,
  PointOfInteraction: String,
  TypeOfInteraction: String
});

var videoSchema1 = mongoose.model('Video', videoSchema, 'video');

app.get('/', (req, res) => {
  videoSchema1.find({},'AccountID UserName TitleID UserAction DateAndTime PointOfInteraction TypeOfInteraction', (err, video) => {
    if(err) return handleError(err);
    res.send(JSON.stringify(video))
  }) 
})

app.post('/',  (req, res) => {
  var awesome_instance = new SomeModel(req.body);
  awesome_instance.save(function (err) {
  if (err) res.send('Error');
    res.send(JSON.stringify(req.body))
  });
})

app.put('/',  (req, res) => {
  res.send('Got a PUT request at /')
})

app.delete('/',  (req, res) => {
  res.send('Got a DELETE request at /')
})

app.listen(port, () => {
 console.log(`Express Application listening at port ` + port)
})
