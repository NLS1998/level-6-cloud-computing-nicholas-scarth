Steps to get the system running and working - 
Type in the command line "sudo docker-compose build" 

Wait until the system has built

Once built. Type "sudo docker-compose up"

Tests to run on the system - 

Look out for the node communication taking place. 

Once the system starts you will see each node sending a message saying its node and the phrase "I am ALIVE" 

To confirm these nodes are actually alive and working through the messaging service. 

You can type "console.log(allTheNodes);" on the last line of the publisher code. All three nodes will be listed in the array.

Look out for a leaderhsip election which will take place on the console and notify you which node has been elected the leader

The leadership election is based off which ID of the node is the biggest. 

To test this write in the code "console.log(allTheNodes);" to confirm the leader has the biggest number. 

The leader is confimed in the array by assigning the number 1 in its leader object. 

Once the system has confirmed in the console that it has elected a leader. 

Open another terminal and delete the non leader node. 

After the system has determined the node down for more than 60 seconds. 

The system will create a new container

To view this container type "sudo docker container ls -a" 

The system is not able to start the container though

Each of the nodes will create a container
